package com.mygdx.game.gamepeices;

import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.gamemap.RoadOr;

/**
 * Created by brandonlanthrip on 4/16/18.
 */
public class Road extends Rectangle {
    public final int x;
    public final int y;
    public final RoadOr orientation;

    public Road(int x, int y, RoadOr o) {
        this.x = x;
        this.y = y;
        orientation = o;
    }
}
