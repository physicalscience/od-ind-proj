package com.mygdx.game.gamepeices;

import com.mygdx.game.gamemap.DiceNumber;
import com.mygdx.game.gamemap.TileResource;
import com.sun.javafx.geom.Rectangle;

/**
 * Created by brandonlanthrip on 4/17/18.
 */
public class GameTile extends Rectangle {
    public final DiceNumber diceNumber;
    public final TileResource type;
    public final static int SIZE = 93;
    public final static int STARTPIECE_X = 100;
    public final static int STARTPIECE_Y = 561;
    public final static int Y_SEPERATOR = 115;
    public final static int X_SEPERATOR = 120;
    public final static int MAX_TILES = 19;
    public final static int THREE_ROW_ADJ_X = 3;
    public final static int THREE_ROW_ADJ_Y = 5;
    public final static int SECOND_ROW_ADJ_X = 64;
    public final static int FIRST_ROW_ADJ_X = GameTile.SECOND_ROW_ADJ_X + 59;
    public final static int FOURTH_ROW_ADJ_X = 60;
    public final static int FOURTH_ROW_ADJ_Y = 8;
    public final static int FIFTH_ROW_ADJ_X = GameTile.SECOND_ROW_ADJ_X + 54;
    public final static int FIFTH_ROW_ADJ_Y = 13;
    public boolean pressed = false;

    public GameTile(DiceNumber diceNumber, TileResource type) {
        this.diceNumber = diceNumber;
        this.type = type;
    }


}
