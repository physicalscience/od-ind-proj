package com.mygdx.game.gamepeices;

import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.gamemap.TownVector;

/**
 * Created by brandonlanthrip on 4/21/18.
 */
public class City extends Rectangle {
    public final TownVector info;

    public City(TownVector info) {
        this.info = info;
    }
}
