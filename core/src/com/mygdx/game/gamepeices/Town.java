package com.mygdx.game.gamepeices;

import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.gamemap.TownVector;

/**
 * Created by brandonlanthrip on 4/16/18.
 */
public class Town extends Rectangle {
    public final TownVector info;

    public Town(TownVector info) {
        this.info = info;
    }
}
