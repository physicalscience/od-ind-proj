package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.mygdx.game.gamemap.*;
import com.mygdx.game.gamemap.TownVector;
import com.mygdx.game.gamepeices.City;
import com.mygdx.game.gamepeices.GameTile;
import com.mygdx.game.gamepeices.Road;
import com.mygdx.game.gamepeices.Town;

import java.util.*;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by brandonlanthrip on 4/16/18.
 */
public class Catan implements Screen {
    Game game;
    Stage stage;
    GameTile lastPressed = null;
    OrthographicCamera cam;
    List<GameTile> tiles = new ArrayList<GameTile>();
    List<Texture> images = new ArrayList<Texture>();
    List<Player> players = new ArrayList<Player>();
    Random diceRoll = new Random();
    Player currentPlayer;
    TownVector firstPlacement;
    static BitmapFont font = new BitmapFont();
    static String lumberCount = ""+0;
    static String oreCount = ""+0;
    static String sheepCount = ""+0;
    static String wheatCount = ""+0;
    static String brickCount = ""+0;
    GameState state;
    SpriteBatch batch;
    static HashMap<String, Texture> pieceTextures = new HashMap<String, Texture>();
    static Texture background = new Texture(Gdx.files.internal("background.png"));
    static Texture dice = new Texture(Gdx.files.internal("dice_button.png"));
    static Texture card = new Texture(Gdx.files.internal("card_button.png"));
    static Texture purchase = new Texture(Gdx.files.internal("money_button.png"));
    static Texture buyMenu = new Texture(Gdx.files.internal("building_cost.png"));
    static Texture openSpot = new Texture(Gdx.files.internal("pointer.png"));
    static Texture blueCity = new Texture(Gdx.files.internal("blue_2.png"));
    static Texture whiteCity = new Texture(Gdx.files.internal("white_2.png"));
    static Texture redCity = new Texture(Gdx.files.internal("red_2.png"));
    static Texture orangeCity = new Texture(Gdx.files.internal("orange_2.png"));
    static Texture placeTown = new Texture(Gdx.files.internal("place_a_town.png"));
    static Texture placeRoad = new Texture(Gdx.files.internal("place_a_road.png"));
    static Texture placeAnotherTown = new Texture(Gdx.files.internal("place_another_town.png"));
    static Texture noDice = new Texture(Gdx.files.internal("x_dice_button.png"));
    static Texture lumberIcon = new Texture(Gdx.files.internal("wood_r.png"));
    static Texture wheatIcon = new Texture(Gdx.files.internal("wheat_r.png"));
    static Texture sheepIcon = new Texture(Gdx.files.internal("sheep_r.png"));
    static Texture oreIcon = new Texture(Gdx.files.internal("ore_r.png"));
    static Texture brickIcon = new Texture(Gdx.files.internal("brick_r.png"));
    static Texture end = new Texture(Gdx.files.internal("end.png"));
    static Texture bluePlayer = new Texture(Gdx.files.internal("blue.png"));
    static Texture redPlayer = new Texture(Gdx.files.internal("red.png"));
    static Texture orangePlayer = new Texture(Gdx.files.internal("orange.png"));
    static Texture two = new Texture(Gdx.files.internal("2.png"));
    static Texture three = new Texture(Gdx.files.internal("3.png"));
    static Texture four = new Texture(Gdx.files.internal("4.png"));
    static Texture five = new Texture(Gdx.files.internal("5.png"));
    static Texture six = new Texture(Gdx.files.internal("6.png"));
    static Texture seven = new Texture(Gdx.files.internal("7.png"));
    static Texture eight = new Texture(Gdx.files.internal("8.png"));
    static Texture nine = new Texture(Gdx.files.internal("9.png"));
    static Texture ten = new Texture(Gdx.files.internal("10.png"));
    static Texture eleven = new Texture(Gdx.files.internal("11.png"));
    static Texture twelve = new Texture(Gdx.files.internal("12.png"));
    static Texture redTurn = new Texture(Gdx.files.internal("red_select.png"));
    static Texture blueTurn = new Texture(Gdx.files.internal("blue_select.png"));
    static Texture orangeTurn = new Texture(Gdx.files.internal("orange_select.png"));
    private boolean buyMenuOpen = false;


    public Catan(Game game) {
        players.add(new Player(0, PlayerColor.White));
        players.add(new Player(1, PlayerColor.Blue));
        players.add(new Player(2, PlayerColor.Red));
        players.add(new Player(3, PlayerColor.Orange));
        currentPlayer = players.get(0);
        state = GameState.FIRST_PLACEMENT;
        this.game = game;
        cam = new OrthographicCamera();
        cam.setToOrtho(false, 800, 800);
        batch = new SpriteBatch();
        int rowCounter = 0;
        int columnCounter = 0;
        int rowAdjustment = GameTile.FIRST_ROW_ADJ_X;
        int yAdjustment = 0;
        HashMap<Integer, TileResource> map = TileResource.getMap();
        HashMap<Integer, DiceNumber> diceMap = DiceNumber.getMap();
        Random r = new Random();

        for(int i = 0; i < GameTile.MAX_TILES; i ++) {
            TileResource resource = TileResource.grabByKey(r.nextInt(TileResource.size), r, map);
            GameTile t = new GameTile(DiceNumber.grabByKey(r.nextInt(DiceNumber.SIZE),r,diceMap, resource), resource);
            t.x = GameTile.STARTPIECE_X + (rowCounter * GameTile.X_SEPERATOR) + rowAdjustment;
            t.y = GameTile.STARTPIECE_Y - (columnCounter * GameTile.Y_SEPERATOR) - yAdjustment;
            t.height = GameTile.SIZE;
            t.width = GameTile.SIZE;
            tiles.add(t);
            switch (columnCounter) {
                case 0:
                    if (rowCounter == 2) {
                        rowCounter = 0;
                        rowAdjustment = GameTile.SECOND_ROW_ADJ_X;
                        columnCounter++;
                    } else {
                        rowCounter++;
                    }
                    break;
                case 1:
                    if (rowCounter == 3) {
                        rowCounter = 0;
                        rowAdjustment = GameTile.THREE_ROW_ADJ_X;
                        yAdjustment = GameTile.THREE_ROW_ADJ_Y;
                        columnCounter++;
                    } else {
                        rowCounter++;
                    }
                    break;
                case 2:
                    if(rowCounter == 4) {
                        rowCounter = 0;
                        yAdjustment = GameTile.FOURTH_ROW_ADJ_Y;
                        rowAdjustment = GameTile.FOURTH_ROW_ADJ_X;
                        columnCounter++;
                    } else {
                        rowCounter++;
                    }
                    break;
                case 3:
                    if (rowCounter == 3) {
                        rowCounter = 0;
                        yAdjustment = GameTile.FIFTH_ROW_ADJ_Y;
                        rowAdjustment = GameTile.FIFTH_ROW_ADJ_X;
                        columnCounter++;
                    } else {
                        rowCounter++;
                    }
                    break;
                case 4:
                    if (rowCounter == 2) {
                        rowCounter = 0;
                        columnCounter++;
                    } else {
                        rowCounter++;
                    }
                    break;
            }

        }

        for (GameTile t : tiles) {
            Texture texture = new Texture(Gdx.files.internal(t.type.fileName));
            Texture pTexture = new Texture(Gdx.files.internal(t.type.pressedFileName));
            pieceTextures.put(t.type.fileName, texture);
            pieceTextures.put(t.type.pressedFileName, pTexture);
            if (t.type != TileResource.DESERT) {
                Texture diceTexture = new Texture(Gdx.files.internal(t.diceNumber.file));
                pieceTextures.put(t.diceNumber.file, diceTexture);
            }
        }
        setCitySlots(state.townSlots);
        setRoadSlots(state.roadSlots);
        startGame();
    }

    private void setCitySlots(List<TownVector> citySlots) {
        citySlots.add(new TownVector(283, 713, new GameTile[]{tiles.get(0)}));
        citySlots.add(new TownVector(402, 713, new GameTile[]{tiles.get(1)}));
        citySlots.add(new TownVector(521, 713, new GameTile[]{tiles.get(2)}));
        citySlots.add(new TownVector(226, 673, new GameTile[]{tiles.get(0)}));
        citySlots.add(new TownVector(343, 673, new GameTile[]{tiles.get(0), tiles.get(1)}));
        citySlots.add(new TownVector(463, 673, new GameTile[]{tiles.get(1), tiles.get(2)}));
        citySlots.add(new TownVector(583, 673, new GameTile[]{tiles.get(2)}));
        citySlots.add(new TownVector(224, 600, new GameTile[]{tiles.get(0), tiles.get(3)}));
        citySlots.add(new TownVector(346, 600, new GameTile[]{tiles.get(0), tiles.get(1), tiles.get(4)}));
        citySlots.add(new TownVector(465, 600, new GameTile[]{tiles.get(1), tiles.get(2), tiles.get(5)}));
        citySlots.add(new TownVector(582, 600, new GameTile[]{tiles.get(2), tiles.get(6)}));
        citySlots.add(new TownVector(165, 560, new GameTile[]{tiles.get(3)}));
        citySlots.add(new TownVector(285, 560, new GameTile[]{tiles.get(0), tiles.get(3), tiles.get(4)}));
        citySlots.add(new TownVector(404, 560, new GameTile[]{tiles.get(1), tiles.get(4), tiles.get(5)}));
        citySlots.add(new TownVector(524, 560, new GameTile[]{tiles.get(2), tiles.get(5), tiles.get(6)}));
        citySlots.add(new TownVector(641, 560, new GameTile[]{tiles.get(6)}));
        citySlots.add(new TownVector(165, 482, new GameTile[]{tiles.get(4), tiles.get(7)}));
        citySlots.add(new TownVector(284, 482, new GameTile[]{tiles.get(4), tiles.get(5), tiles.get(8)}));
        citySlots.add(new TownVector(403, 482, new GameTile[]{tiles.get(5), tiles.get(6), tiles.get(9)}));
        citySlots.add(new TownVector(525, 482, new GameTile[]{tiles.get(5), tiles.get(6), tiles.get(10)}));
        citySlots.add(new TownVector(644, 482, new GameTile[]{tiles.get(6), tiles.get(11)}));
        citySlots.add(new TownVector(104, 441, new GameTile[]{tiles.get(7)}));
        citySlots.add(new TownVector(224, 441, new GameTile[]{tiles.get(3), tiles.get(7), tiles.get(8)}));
        citySlots.add(new TownVector(342, 441, new GameTile[]{tiles.get(4), tiles.get(8), tiles.get(9)}));
        citySlots.add(new TownVector(463, 441, new GameTile[]{tiles.get(5), tiles.get(9), tiles.get(10)}));
        citySlots.add(new TownVector(584, 441, new GameTile[]{tiles.get(6), tiles.get(10), tiles.get(11)}));
        citySlots.add(new TownVector(701, 441, new GameTile[]{tiles.get(11)}));
        citySlots.add(new TownVector(103, 364, new GameTile[]{tiles.get(7)}));
        citySlots.add(new TownVector(223, 364, new GameTile[]{tiles.get(7), tiles.get(8), tiles.get(12)}));
        citySlots.add(new TownVector(340, 364, new GameTile[]{tiles.get(8), tiles.get(9), tiles.get(13)}));
        citySlots.add(new TownVector(463, 364, new GameTile[]{tiles.get(9), tiles.get(10), tiles.get(14)}));
        citySlots.add(new TownVector(582, 364, new GameTile[]{tiles.get(10), tiles.get(11), tiles.get(15)}));
        citySlots.add(new TownVector(701, 364, new GameTile[]{tiles.get(11)}));
        citySlots.add(new TownVector(161, 324, new GameTile[]{tiles.get(7), tiles.get(12)}));
        citySlots.add(new TownVector(279, 324, new GameTile[]{tiles.get(8), tiles.get(12), tiles.get(13)}));
        citySlots.add(new TownVector(401, 324, new GameTile[]{tiles.get(9), tiles.get(13), tiles.get(14)}));
        citySlots.add(new TownVector(520, 324, new GameTile[]{tiles.get(10), tiles.get(14), tiles.get(15)}));
        citySlots.add(new TownVector(640, 324, new GameTile[]{tiles.get(11), tiles.get(15)}));
        citySlots.add(new TownVector(161, 245, new GameTile[]{tiles.get(12)}));
        citySlots.add(new TownVector(280, 245, new GameTile[]{tiles.get(12), tiles.get(13), tiles.get(16)}));
        citySlots.add(new TownVector(401, 245, new GameTile[]{tiles.get(13), tiles.get(14), tiles.get(17)}));
        citySlots.add(new TownVector(518, 245, new GameTile[]{tiles.get(14), tiles.get(15), tiles.get(18)}));
        citySlots.add(new TownVector(638, 245, new GameTile[]{tiles.get(15)}));
        citySlots.add(new TownVector(218, 205, new GameTile[]{tiles.get(12), tiles.get(16)}));
        citySlots.add(new TownVector(318, 205, new GameTile[]{tiles.get(13), tiles.get(16), tiles.get(17)}));
        citySlots.add(new TownVector(458, 205, new GameTile[]{tiles.get(14), tiles.get(17), tiles.get(18)}));
        citySlots.add(new TownVector(579, 205, new GameTile[]{tiles.get(15), tiles.get(18)}));
        citySlots.add(new TownVector(218, 126, new GameTile[]{tiles.get(16)}));
        citySlots.add(new TownVector(339, 126, new GameTile[]{tiles.get(16), tiles.get(17)}));
        citySlots.add(new TownVector(458, 126, new GameTile[]{tiles.get(17), tiles.get(18)}));
        citySlots.add(new TownVector(576, 126, new GameTile[]{tiles.get(18)}));
        citySlots.add(new TownVector(279, 88, new GameTile[]{tiles.get(16)}));
        citySlots.add(new TownVector(399, 88, new GameTile[]{tiles.get(17)}));
        citySlots.add(new TownVector(519, 88, new GameTile[]{tiles.get(18)}));
    }

    private void setRoadSlots(List<RoadVector> roadSlots) {
        roadSlots.add(new RoadVector(251, 694, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(315, 694, RoadOr.LEFT));
        roadSlots.add(new RoadVector(375, 694, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(432, 694, RoadOr.LEFT));
        roadSlots.add(new RoadVector(494, 694, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(551, 694, RoadOr.LEFT));
        roadSlots.add(new RoadVector(224, 636, RoadOr.VERT));
        roadSlots.add(new RoadVector(343, 636, RoadOr.VERT));
        roadSlots.add(new RoadVector(464, 636, RoadOr.VERT));
        roadSlots.add(new RoadVector(582, 636, RoadOr.VERT));
        roadSlots.add(new RoadVector(192, 582, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(253, 582, RoadOr.LEFT));
        roadSlots.add(new RoadVector(319, 582, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(373, 582, RoadOr.LEFT));
        roadSlots.add(new RoadVector(437, 582, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(494, 582, RoadOr.LEFT));
        roadSlots.add(new RoadVector(557, 582, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(614, 582, RoadOr.LEFT));
        roadSlots.add(new RoadVector(165, 524, RoadOr.VERT));
        roadSlots.add(new RoadVector(284, 524, RoadOr.VERT));
        roadSlots.add(new RoadVector(404, 524, RoadOr.VERT));
        roadSlots.add(new RoadVector(524, 524, RoadOr.VERT));
        roadSlots.add(new RoadVector(643, 524, RoadOr.VERT));
        roadSlots.add(new RoadVector(135, 464, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(194, 464, RoadOr.LEFT));
        roadSlots.add(new RoadVector(257, 464, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(313, 464, RoadOr.LEFT));
        roadSlots.add(new RoadVector(375, 464, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(435, 464, RoadOr.LEFT));
        roadSlots.add(new RoadVector(500, 464, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(559, 464, RoadOr.LEFT));
        roadSlots.add(new RoadVector(614, 464, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(675, 464, RoadOr.LEFT));
        roadSlots.add(new RoadVector(103, 406, RoadOr.VERT));
        roadSlots.add(new RoadVector(222, 406, RoadOr.VERT));
        roadSlots.add(new RoadVector(343, 406, RoadOr.VERT));
        roadSlots.add(new RoadVector(464, 406, RoadOr.VERT));
        roadSlots.add(new RoadVector(583, 406, RoadOr.VERT));
        roadSlots.add(new RoadVector(702, 406, RoadOr.VERT));
        roadSlots.add(new RoadVector(127, 345, RoadOr.LEFT));
        roadSlots.add(new RoadVector(195, 345, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(257, 345, RoadOr.LEFT));
        roadSlots.add(new RoadVector(314, 345, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(371, 345, RoadOr.LEFT));
        roadSlots.add(new RoadVector(435, 345, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(495, 345, RoadOr.LEFT));
        roadSlots.add(new RoadVector(555, 345, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(615, 345, RoadOr.LEFT));
        roadSlots.add(new RoadVector(669, 345, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(161, 284, RoadOr.VERT));
        roadSlots.add(new RoadVector(281, 284, RoadOr.VERT));
        roadSlots.add(new RoadVector(399, 284, RoadOr.VERT));
        roadSlots.add(new RoadVector(519, 284, RoadOr.VERT));
        roadSlots.add(new RoadVector(639, 284, RoadOr.VERT));
        roadSlots.add(new RoadVector(186, 228, RoadOr.LEFT));
        roadSlots.add(new RoadVector(253, 228, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(310, 228, RoadOr.LEFT));
        roadSlots.add(new RoadVector(371, 228, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(431, 228, RoadOr.LEFT));
        roadSlots.add(new RoadVector(490, 228, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(548, 228, RoadOr.LEFT));
        roadSlots.add(new RoadVector(608, 228, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(217, 165, RoadOr.VERT));
        roadSlots.add(new RoadVector(338, 165, RoadOr.VERT));
        roadSlots.add(new RoadVector(458, 165, RoadOr.VERT));
        roadSlots.add(new RoadVector(577, 165, RoadOr.VERT));
        roadSlots.add(new RoadVector(247, 105, RoadOr.LEFT));
        roadSlots.add(new RoadVector(311, 105, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(369, 105, RoadOr.LEFT));
        roadSlots.add(new RoadVector(431, 105, RoadOr.RIGHT));
        roadSlots.add(new RoadVector(488, 105, RoadOr.LEFT));
        roadSlots.add(new RoadVector(548, 105, RoadOr.RIGHT));
    }

    private void startGame() {

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cam.update();

        batch.setProjectionMatrix(cam.combined);
        batch.begin();
        batch.draw(background, 0, 0, 800, 800);
        if(currentPlayer.color == PlayerColor.Blue) {
            batch.draw(blueTurn, 670, 630, 800 * 0.15f, 800 * 0.15f);
        } else {
            batch.draw(bluePlayer, 670, 630, 800 * 0.15f, 800 * 0.15f);
        }
        if(currentPlayer.color == PlayerColor.Orange) {
            batch.draw(orangeTurn, 3, 50, 800 * 0.15f, 800 * 0.15f);
        } else {
            batch.draw(orangePlayer, 3, 50, 800 * 0.15f, 800 * 0.15f);
        }
        if(currentPlayer.color == PlayerColor.Red) {
            batch.draw(redTurn, 3, 635, 800 * 0.15f, 800 * 0.15f);
        } else {
            batch.draw(redPlayer, 3, 635, 800 * 0.15f, 800 * 0.15f);
        }


        for (int i = 0; i < tiles.size(); i++) {
            GameTile t = tiles.get(i);
            Texture texture;
            if(t.pressed) {
                texture = pieceTextures.get(t.type.pressedFileName);
            } else {
                texture = pieceTextures.get(t.type.fileName);
            }
            batch.draw(texture, t.x, t.y, 800 * 0.15f, 800 * 0.2f);
            if (t.type != TileResource.DESERT) {
                Texture dice = pieceTextures.get(t.diceNumber.file);
                batch.draw(dice, t.x + 35 ,t.y + 45, 800 * 0.06f, 800 * 0.08f);
            }
            images.add(texture);
        }
        if(state == GameState.DICE_ROLL) {
            batch.draw(dice, 730, 190, 800 * 0.08f, 800 * 0.1f);
        } else {
            batch.draw(noDice, 730, 190, 800 * .08f, 800 * .1f);
        }

        final float iconWidth = 800 * 0.04f;
        final float iconLength = 800 * 0.06f;

        batch.draw(end, 755, 270, 800 * 0.055f, 800 * 0.055f);
        batch.draw(card, 730, 105, 800 * 0.08f, 800 * 0.1f);
        batch.draw(purchase, 730, 20, 800 * 0.08f, 800 * 0.1f);
        batch.draw(oreIcon, 658, 102, iconWidth, iconLength);
        batch.draw(sheepIcon, 630, 50, iconWidth, iconLength);
        batch.draw(brickIcon, 630, 2, iconWidth, iconLength);
        batch.draw(lumberIcon, 678, 50, iconWidth, iconLength);
        batch.draw(wheatIcon, 678, 2, iconWidth, iconLength);
        font.draw(batch, oreCount, 695, 127);
        font.draw(batch, sheepCount, 665, 80);
        font.draw(batch, lumberCount, 715, 80);
        font.draw(batch, brickCount, 665, 32);
        font.draw(batch, wheatCount, 715, 32);

        switch (state) {
            case FIRST_PLACEMENT:
            case PLACE_TOWN:
                batch.draw(placeTown, 270, 8, 800 * .5f, 800 * .1f);
                Iterator<TownVector> t = GameState.townSlots.iterator();
                while(t.hasNext()) {
                    TownVector v = t.next();
                    int x = v.x;
                    int y = v.y;
                    batch.draw(openSpot, x - 35, y - 20);
                }
                break;

            case ROAD_PLACEMENT:
            case PLACE_ROAD:
                batch.draw(placeRoad, 270, 10, 800 *.5f, 800 * .1f);
                Iterator<RoadVector> r = GameState.roadSlots.iterator();
                while(r.hasNext()) {
                    RoadVector v = r.next();
                    batch.draw(openSpot, v.x - 35, v.y - 20);
                }
                break;
            case SECOND_PLACEMENT:
                batch.draw(placeAnotherTown, 200, 10, 800 *.5f, 800 * .1f);
                Iterator<TownVector> two = GameState.townSlots.iterator();
                while(two.hasNext()) {
                    TownVector v = two.next();
                    batch.draw(openSpot, v.x - 35, v.y - 20);
                }
                break;
            case PLACE_CITY:
                Iterator<TownVector> c = currentPlayer.towns.stream()
                        .map(s -> s.info)
                        .collect(Collectors.toList()).iterator();
                while(c.hasNext()) {
                    TownVector v = c.next();
                    batch.draw(openSpot, v.x - 35, v.y - 20);
                }
        }


        for(Player p : players) {
            for (Road r : p.roads) {
                int x = r.x - 30;
                int y = r.y - 20;
                float w = 800 * .07f;
                float h = 800 * .07f;

                if (r.orientation == RoadOr.FLAT) {
                    batch.draw(p.color.roadFlat, x, y, w, h);
                } else if (r.orientation == RoadOr.LEFT) {
                    batch.draw(p.color.roadLeft, x, y, w, h);
                } else if (r.orientation == RoadOr.RIGHT) {
                    batch.draw(p.color.roadRight, x, y, w, h);
                } else if (r.orientation == RoadOr.VERT) {
                    batch.draw(p.color.roadVert, x, y, w, h);
                }
            }
           for (Town t : p.towns) {
               batch.draw(p.color.town, t.info.x - 30, t.info.y - 20, 800 * .07f, 800 * .07f);
           }
           for (City c : p.cities) {
               batch.draw(p.color.city, c.x, c.y);
           }

        }
        if(buyMenuOpen) {
            batch.draw(buyMenu, 250, 200, 800 * 0.34f, 800 * 0.5f);
        }


        if(Gdx.input.isTouched()) {
            Vector3 point = new Vector3();
            point.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            cam.unproject(point);
            selectSomething((int)point.x, (int)point.y);
        }
        batch.end();
    }

    private void selectSomething(int x, int y) {
        if ((x >= 731 && x <= 792) && (y >= 20 && y <= 94)) {
            buyMenuOpen = true;
        } else if (buyMenuOpen && ((x < 254 || x > 520) || (y > 595 || y < 200))) {
            buyMenuOpen = false;
        }

        switch (state) {
            case FIRST_PLACEMENT:
                Iterator<TownVector> t = GameState.townSlots.iterator();
                placeTown(t ,x ,y, GameState.ROAD_PLACEMENT);
                changePlayer(currentPlayer.id);
                break;
            case ROAD_PLACEMENT:
                Iterator<RoadVector> r = GameState.roadSlots.iterator();
                placeRoad(r, x, y, GameState.SECOND_PLACEMENT);
                changePlayer(currentPlayer.id);
                break;
            case SECOND_PLACEMENT:
                Iterator<TownVector> v = GameState.townSlots.iterator();
                placeTown(v, x, y, GameState.DICE_ROLL);
                changePlayer(currentPlayer.id);
                break;
            case DICE_ROLL:
                if((x >= 731 && x <= 792) && (y >= 193 && y <= 265)) {
                    int roll = rollDice(diceRoll.nextInt(12));
                    showDiceRoll(roll);
                    for(Player p : players) {
                        for(Town town : p.towns) {
                            for(GameTile tile : town.info.tile) {
                                if(tile.diceNumber.value == roll) {
                                    p.resources.add(CardResource.getResource(tile.type));
                                    switch (tile.type) {
                                        case ORE:
                                            oreCount = convertIncrement(Integer.parseInt(oreCount));
                                            break;
                                        case SHEEP:
                                            sheepCount = convertIncrement(Integer.parseInt(sheepCount));
                                            break;
                                        case WHEAT:
                                            wheatCount = convertIncrement(Integer.parseInt(wheatCount));
                                            break;
                                        case WOOD:
                                            lumberCount = convertIncrement(Integer.parseInt(lumberCount));
                                            break;
                                        case BRICK:
                                            brickCount = convertIncrement(Integer.parseInt(brickCount));
                                    }
                                }
                            }
                        }
                    }
                    state = GameState.PLAYER_TURN;
                }
                break;
            case PLAYER_TURN:
                if(buyMenuOpen) {
                    if(x >= 437 && x <= 490) {
                        if(y >= 485 && y <= 514) {
                            purchaseRoad();
                            buyMenuOpen = false;
                        } else if(y >= 423 && y <= 456) {
                            purchaseSettlement();
                            buyMenuOpen = false;
                        } else if(y >= 364 && y <= 396) {
                            purchaseCity();
                            buyMenuOpen = false;
                        } else if(y >= 305 && y <= 338) {
                            purchaseCard();
                            buyMenuOpen = false;
                        }
                    }
                } else if((x >= 756 && x <= 794) && (y >= 277 && y <= 309)) {
                    changePlayer(currentPlayer.id);
                    state = GameState.DICE_ROLL;
                }
                break;
            case PLACE_ROAD:
                Iterator<RoadVector> roadVector = GameState.roadSlots.iterator();
                placeRoad(roadVector, x, y, GameState.PLAYER_TURN);
                break;

            case PLACE_CITY:
                Iterator<Town> towns = currentPlayer.towns.iterator();
                placeCity(towns, x, y, GameState.PLAYER_TURN);
                break;

            case PLACE_TOWN:
                Iterator<Town> placeT = currentPlayer.towns.iterator();
                placeCity(placeT, x, y, GameState.PLAYER_TURN);
                break;
        }
    }

    private void changePlayer(int pId) {
        switch (pId) {
            case 0:
            case 1:
            case 2:
                currentPlayer = players.get(pId + 1);
                break;
            case 3:
                currentPlayer = players.get(0);
                switch (state) {
                    case FIRST_PLACEMENT:
                        state = GameState.ROAD_PLACEMENT;
                        break;
                    case ROAD_PLACEMENT:
                        state = GameState.SECOND_PLACEMENT;
                        break;
                    case SECOND_PLACEMENT:
                        state = GameState.DICE_ROLL;
                        break;
                }
                break;
        }
        oreCount = String.valueOf((int)currentPlayer.resources.stream().filter(o -> o.key == CardResource.ORE.key).count());
        sheepCount = String.valueOf((int)currentPlayer.resources.stream().filter(o -> o.key == CardResource.SHEEP.key).count());
        wheatCount = String.valueOf((int)currentPlayer.resources.stream().filter(o -> o.key == CardResource.WHEAT.key).count());
        lumberCount = String.valueOf((int)currentPlayer.resources.stream().filter(o -> o.key == CardResource.WOOD.key).count());
        brickCount = String.valueOf((int)currentPlayer.resources.stream().filter(o -> o.key == CardResource.BRICK.key).count());
    }

    private void showDiceRoll(int number) {
        Texture show;
        switch (number) {
            case 2:
                show = two;
                break;
            case 3:
                show = three;
                break;
            case 4:
                show = four;
                break;
            case 5:
                show = five;
                break;
            case 6:
                show = six;
                break;
            case 7:
                show = seven;
                break;
            case 8:
                show = eight;
                break;
            case 9:
                show = nine;
                break;
            case 10:
                show = ten;
                break;
            case 11:
                show = eleven;
                break;
            case 12:
                show = twelve;
                break;
            default:
                show = null;
                break;
        }
        batch.draw(show, 200, 200, 800 * .5f, 800 * .5f);
    }

    private void purchaseCard() {
        if(currentPlayer.resources.stream()
                .filter(w -> w == CardResource.WHEAT).count() >= 1 &&
                currentPlayer.resources.stream()
                .filter(s -> s == CardResource.SHEEP).count() >= 1 &&
                currentPlayer.resources.stream()
                .filter(o -> o == CardResource.ORE).count() >= 1) {
            state = GameState.BUY_CARD;
            removeCards(1, CardResource.WHEAT);
            removeCards(1, CardResource.SHEEP);
            removeCards(1, CardResource.ORE);
            wheatCount = convertDecrement(Integer.parseInt(wheatCount));
            oreCount = convertDecrement(Integer.parseInt(oreCount));
            sheepCount = convertDecrement(Integer.parseInt(sheepCount));
        }
    }

    private void purchaseCity() {
        if(currentPlayer.resources.stream()
                .filter(w -> w == CardResource.WHEAT).count() >= 2 &&
                currentPlayer.resources.stream()
                .filter(o -> o == CardResource.ORE).count() >= 3) {
            state = GameState.PLACE_CITY;
            removeCards(2, CardResource.WHEAT);
            removeCards(3, CardResource.ORE);
            for(int i = 0; i < 3; i++) { oreCount = convertDecrement(Integer.parseInt(oreCount)); }
            for(int i = 0; i < 2; i++) { wheatCount = convertDecrement(Integer.parseInt(wheatCount)); }
        }
    }

    private void purchaseSettlement() {
        if(currentPlayer.resources.stream()
                .filter(w -> w == CardResource.WOOD).count() >= 1 &&
                currentPlayer.resources.stream()
                .filter(b -> b == CardResource.BRICK).count() >= 1 &&
                currentPlayer.resources.stream()
                .filter(wh -> wh == CardResource.WHEAT).count() >= 1 &&
                currentPlayer.resources.stream()
                .filter(s -> s == CardResource.SHEEP).count() >= 1) {
            state = GameState.PLACE_TOWN;
            removeCards(1, CardResource.WOOD);
            removeCards(1, CardResource.BRICK);
            removeCards(1, CardResource.WHEAT);
            removeCards(1, CardResource.SHEEP);
            lumberCount = convertDecrement(Integer.parseInt(lumberCount));
            brickCount = convertDecrement(Integer.parseInt(brickCount));
            wheatCount = convertDecrement(Integer.parseInt(wheatCount));
            sheepCount = convertDecrement(Integer.parseInt(sheepCount));
        }

    }
    private void purchaseRoad() {
        if(currentPlayer.resources.stream()
                .filter( r -> r == CardResource.WOOD).count() >= 1 &&
        currentPlayer.resources.stream()
                .filter(b -> b == CardResource.BRICK).count() >= 1) {
            state = GameState.PLACE_ROAD;
            removeCards(1, CardResource.WOOD);
            removeCards(1, CardResource.BRICK);
            lumberCount = convertDecrement(Integer.parseInt(lumberCount));
            brickCount = convertDecrement(Integer.parseInt(brickCount));
        }
    }


    private void removeCards(int n, CardResource resource) {
        Iterator<CardResource> cards = currentPlayer.resources.iterator();
        while(cards.hasNext()) {
            CardResource r = cards.next();
            if(r == resource) {
                cards.remove();
                n--;
                if(n == 0) {
                    break;
                }
            }
        }
    }

    private String convertIncrement(int x) {
        x++;
        return String.valueOf(x);
    }

    private String convertDecrement(int x) {
        x--;
        return String.valueOf(x);
    }

    private int rollDice(int r) {
        if (r >= 2) return r;
        else return rollDice(diceRoll.nextInt(12));
    }

    private void placeTown(Iterator<TownVector> v, int x, int y, GameState next) {
        while(v.hasNext()) {
            TownVector s = v.next();
            if((x >= s.x - 35 && x <= s.x + 35) && (y >= s.y - 20 && y <= s.y +20)) {
                currentPlayer.addToTown(new Town(new TownVector(s.x, s.y, s.tile)));
                v.remove();
                if(state == GameState.PLACE_TOWN) {
                    state = next;
                }

            }
        }
    }

    private void placeCity(Iterator<Town> v, int x, int y, GameState next) {
        while(v.hasNext()) {
            Town s = v.next();
            if((x >= s.info.x - 35 && x <= s.info.x +35) && (y >= s.info.y - 20 && y <= s.info.y + 20)) {
                currentPlayer.addToCity(new City(s.info), s);
                if(state == GameState.PLACE_CITY) {
                    state = next;
                }
            }
        }
    }

    private void placeRoad(Iterator<RoadVector> r, int x, int y, GameState next) {
        while(r.hasNext()) {
            RoadVector v = r.next();
            if((x >= v.x - 35 && x <= v.x + 35) && (y >= v.y - 20 && y <= v.y + 20)) {
                currentPlayer.addToRoads(new Road(v.x, v.y, v.orientation));
                r.remove();
                if(state == GameState.PLACE_ROAD) {
                    state = next;
                }
            }
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        for(Texture t : images) {
            t.dispose();
        }
        batch.dispose();
    }
}
