package com.mygdx.game.gamemap;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by brandonlanthrip on 4/16/18.
 */
public enum TileResource {
    WOOD("lumber.png", "lumber_select.png", 0, 4),
    SHEEP("sheep.png", "sheep_select.png", 1, 4),
    BRICK("brick.png", "brick_select.png", 2, 3),
    ORE("ore.png", "ore_select.png", 3, 3),
    WHEAT("wheat.png", "wheat_select.png", 4, 4),
    DESERT("desert.png", "desert_select.png", 5, 1);

    public final String fileName;
    public final String pressedFileName;
    public static final int size = 6;
    public final int key;
    public final int amount;
    private int used;

    TileResource(String fileName, String pFileName, int key, int amount) {
        this.fileName = fileName;
        this.pressedFileName = pFileName;
        this.key = key;
        this.amount = amount;
        this.used = 0;
    }

    public static HashMap<Integer, TileResource> getMap() {
        HashMap<Integer, TileResource> map = new HashMap<Integer, TileResource>();
        map.put(0, TileResource.WOOD);
        map.put(1, TileResource.SHEEP);
        map.put(2, TileResource.BRICK);
        map.put(3, TileResource.ORE);
        map.put(4, TileResource.WHEAT);
        map.put(5, TileResource.DESERT);
        return map;
    }


    public static TileResource getByKey(int key) {
        switch (key) {
            case 0:
                return WOOD;
            case 1:
                return SHEEP;
            case 2:
                return BRICK;
            case 3:
                return ORE;
            case 4:
                return WHEAT;
            case 5:
                return DESERT;
            default:
                return null;
        }
    }

    public static TileResource grabByKey(int key, Random random, HashMap<Integer, TileResource> resourceMap) {
        TileResource r = resourceMap.get(key);
        if (r.amount > r.used) {
            r.used ++;
            return r;
        } else {
            return grabByKey(random.nextInt(TileResource.size), random, resourceMap);

        }
    }
}
