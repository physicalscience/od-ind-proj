package com.mygdx.game.gamemap;

import com.mygdx.game.Player;
import com.mygdx.game.gamepeices.Road;
import com.mygdx.game.gamepeices.Town;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brandonlanthrip on 4/21/18.
 */
public enum  GameState {
    FIRST_PLACEMENT(0),
    ROAD_PLACEMENT(1),
    SECOND_PLACEMENT(2),
    DICE_ROLL(3),
    PLAYER_TURN(4),
    PLACE_TOWN(5),
    PLACE_ROAD(6),
    PLACE_CITY(7),
    BUY_CARD(8),
    END_STATE(9);

    public final int key;

    GameState(int key) {
        this.key = key;
    }

    public static GameState getByKey(int key) {
        switch (key) {
            case 0:
                return FIRST_PLACEMENT;
            case 1:
                return ROAD_PLACEMENT;
            case 2:
                return SECOND_PLACEMENT;
            case 3:
                return DICE_ROLL;
            case 4:
                return PLAYER_TURN;
            case 5:
                return PLACE_TOWN;
            case 6:
                return PLACE_ROAD;
            case 7:
                return PLACE_CITY;
            case 8:
                return BUY_CARD;
            case 9:
                return END_STATE;
            default:
                return null;
        }
    }

    private static Player currentPlayer;
    public static final List<TownVector> townSlots = new ArrayList<TownVector>();
    public static final List<RoadVector> roadSlots = new ArrayList<RoadVector>();
    public static List<Town> towns = new ArrayList<Town>();
    public static List<Road> roads = new ArrayList<Road>();

    public void setPlayer(Player player) {
        currentPlayer = player;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }



}
