package com.mygdx.game.gamemap;

import com.mygdx.game.gamepeices.Road;

/**
 * Created by brandonlanthrip on 4/24/18.
 */
public class RoadVector {
    public final int x;
    public final int y;
    public final RoadOr orientation;

    public RoadVector(int x, int y, RoadOr orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }
}
