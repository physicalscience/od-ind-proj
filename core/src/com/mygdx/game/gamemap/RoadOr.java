package com.mygdx.game.gamemap;

/**
 * Created by brandonlanthrip on 4/23/18.
 */
public enum RoadOr {
    RIGHT("road_2.png", 0),
    LEFT("road_3.png", 1),
    FLAT("_road_1.png", 2),
    VERT("_road_4.png", 3);

    public final String file;
    public final int key;

    RoadOr(String file, int key) {
        this.file = file;
        this.key = key;
    }

    public static RoadOr getByKey(int key) {
        switch (key) {
            case 0:
                return RIGHT;
            case 1:
                return LEFT;
            case 2:
                return FLAT;
            case 3:
                return VERT;
            default:
                return null;
        }
    }
}
