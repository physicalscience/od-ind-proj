package com.mygdx.game.gamemap;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by brandonlanthrip on 4/17/18.
 */
public enum  DiceNumber {
    TWO("token_2.png", 1, 0, 2),
    THREE("token_3.png", 2, 1, 3),
    FOUR("token_4.png", 2, 2, 4),
    FIVE("token_5.png", 2, 3, 5),
    SIX("token_6.png", 2, 4, 6),
    EIGHT("token_8.png", 2, 5, 8),
    NINE("token_9.png", 2, 6, 9),
    TEN("token_10.png", 2, 7, 10),
    ELEVEN("token_11.png", 2, 8, 11),
    TWELVE("token_12.png", 1, 9, 12),
    NULL("", 0, 10, 0);

    public final String file;
    public final int amount;
    public final int key;
    private int used;
    public final int value;
    public static final int SIZE = 10;

    DiceNumber(String file, int amount, int key, int value) {
        this.file = file;
        this.amount = amount;
        this.used = 0;
        this.key = key;
        this.value = value;
    }

    public static DiceNumber findByKey(int key) {
        switch (key) {
            case 0:
                return TWO;
            case 1:
                return THREE;
            case 2:
                return FOUR;
            case 3:
                return FIVE;
            case 4:
                return SIX;
            case 5:
                return EIGHT;
            case 6:
                return NINE;
            case 7:
                return TEN;
            case 8:
                return ELEVEN;
            case 9:
                return TWELVE;
            default:
                return NULL;
        }
    }

    public static HashMap<Integer, DiceNumber> getMap() {
        HashMap<Integer, DiceNumber> map = new HashMap<Integer, DiceNumber>();
        map.put(0, DiceNumber.TWO);
        map.put(1, DiceNumber.THREE);
        map.put(2, DiceNumber.FOUR);
        map.put(3, DiceNumber.FIVE);
        map.put(4, DiceNumber.SIX);
        map.put(5, DiceNumber.EIGHT);
        map.put(6, DiceNumber.NINE);
        map.put(7, DiceNumber.TEN);
        map.put(8, DiceNumber.ELEVEN);
        map.put(9, DiceNumber.TWELVE);
        return map;
    }

    public static DiceNumber grabByKey(int key, Random random, HashMap<Integer, DiceNumber> diceMap, TileResource resource) {
        DiceNumber r = diceMap.get(key);
        if (resource == TileResource.DESERT) {
            return DiceNumber.NULL;
        }
        if (r.amount > r.used) {
            r.used ++;
            return r;
        } else {
            return grabByKey(random.nextInt(DiceNumber.SIZE), random, diceMap, resource);

        }
    }
}
