package com.mygdx.game.gamemap;

import javax.smartcardio.Card;

/**
 * Created by brandonlanthrip on 4/23/18.
 */
public enum CardResource {
    WOOD(0),
    SHEEP(1),
    BRICK(2),
    WHEAT(3),
    ORE(4);

    public final int key;

    CardResource(int key) {
        this.key = key;
    }

    public static CardResource getByKey(int key) {
        switch (key) {
            case 0:
                return WOOD;
            case 1:
                return SHEEP;
            case 2:
                return BRICK;
            case 3:
                return WHEAT;
            case 4:
                return ORE;
            default:
                return null;
        }
    }
    public static CardResource getResource(TileResource r) {
        switch (r) {
            case BRICK:
                return CardResource.BRICK;
            case SHEEP:
                return CardResource.SHEEP;
            case WOOD:
                return CardResource.WOOD;
            case WHEAT:
                return CardResource.WHEAT;
            case ORE:
                return CardResource.ORE;
            default:
                return null;
        }
    }
}
