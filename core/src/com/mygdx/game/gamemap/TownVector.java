package com.mygdx.game.gamemap;

import com.mygdx.game.gamepeices.GameTile;

/**
 * Created by brandonlanthrip on 4/23/18.
 */
public class TownVector {
    public final int x;
    public final int y;
    public final GameTile[] tile;

    public TownVector(int x, int y, GameTile[] tile) {
        this.x = x;
        this.y = y;
        this.tile = tile;
    }
}
