package com.mygdx.game.gamemap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by brandonlanthrip on 4/23/18.
 */
public enum PlayerColor {
    White("white_1.png", "white_2.png", "white_road_1.png", "white_road_3.png", "white_road_2.png", "white_road_4.png"),
    Orange("orange_1.png", "orange_2.png", "orange_road_1.png", "orange_road_3.png", "orange_road_2.png", "orange_road_4.png"),
    Blue("blue_1.png", "blue_2.png", "blue_road_1.png", "blue_road_3.png", "blue_road_2.png", "blue_road_4.png"),
    Red("red_1.png", "red_2.png", "red_road_1.png", "red_road_3.png", "red_road_2.png", "red_road_4.png");

    public final Texture city;
    public final Texture town;
    public final Texture roadFlat;
    public final Texture roadRight;
    public final Texture roadLeft;
    public final Texture roadVert;

    PlayerColor(String city, String town, String roadFlat, String roadLeft, String roadRight, String roadVert) {
        this.city = new Texture(Gdx.files.internal(city));
        this.town = new Texture(Gdx.files.internal(town));
        this.roadFlat = new Texture(Gdx.files.internal(roadFlat));
        this.roadLeft = new Texture(Gdx.files.internal(roadLeft));
        this.roadRight = new Texture(Gdx.files.internal(roadRight));
        this.roadVert = new Texture(Gdx.files.internal(roadVert));
    }
}
