package com.mygdx.game;

import com.mygdx.game.gamemap.CardResource;
import com.mygdx.game.gamemap.Cards;
import com.mygdx.game.gamemap.PlayerColor;
import com.mygdx.game.gamepeices.City;
import com.mygdx.game.gamepeices.Road;
import com.mygdx.game.gamepeices.Town;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brandonlanthrip on 4/16/18.
 */
public class Player {
    public final int id;
    public int points = 0;
    public List<Road> roads = new ArrayList<Road>();
    public List<Town> towns = new ArrayList<Town>();
    public List<City> cities = new ArrayList<City>();
    public List<CardResource> resources = new ArrayList<CardResource>();
    public List <Cards> cards = new ArrayList<Cards>();
    public final PlayerColor color;

    public Player(int id, PlayerColor color) {
        this.id = id;
        this.color = color;
    }

    public void addToRoads(Road road) {
        roads.add(road);
    }

    public Road getRoad(int i) {
        return roads.get(i);
    }

    public void addToTown(Town town) {
        towns.add(town);
    }

    public Town getTown(int i) {
        return towns.get(i);
    }

    public void addToCity(City city, Town town) {
        towns.remove(town);
        cities.add(city);
    }

    public City getCity(int i) {
        return cities.get(i);
    }

    public int viewPoints() { return points; }

    public int addPoint() {
        points++;
        return points;
    }

    public List<CardResource> viewCards() {
        return resources;
    }

}
